<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP4 - Welcome</title>
</head>
<body>
    <?php
        session_start();

        if(isset($_POST['logout'])) {
            $_SESSION['connected'] = false;
        }

        if(!isset($_SESSION['connected']) || $_SESSION['connected'] != true) {
            header("Location: login.php");
        }
    ?>
    <h1>Bienvenue!</h1>
    <form action="#" method="POST">
        <input type="hidden" name="logout" value="logout">
        <input type="submit" value="Logout">
    </form>
    <?php
        require_once 'class/ForumModel.php';
        
        echo "<ul>";
        foreach (ForumModel::getForums() as $forum) {
            echo "<li><a href=\"forums.php?id=".$forum[0]."\">".$forum[1]." - (#".$forum[2].")</a></li>";
        }
        echo "</ul>";
    ?>
</body>
</html>