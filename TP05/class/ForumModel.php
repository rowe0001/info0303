<?php
    include 'class/Forum.php';
    include 'class/MyPDO.php';

    final class ForumModel {

        public static function createForum(Forum $forum) : int {
            $SQL = <<<SQL
                INSERT INTO forum(title, category) VALUES(:title, :category);
            SQL;

            $DB = MyPDO::getInstance();
            if($request = $DB->prepare($SQL)) {
                $request->execute([
                    ':title'=>$forum->getTitle(),
                    ':category'=>$forum->getCategory()
                ]);

                return $DB->lastInsertId();
            }

            return -1;
        }

        public static function getForum(int $id) : ?Forum {
            $SQL = <<<SQL
                SELECT id, title, category FROM forum WHERE id=:id
            SQL;

            $DB = MyPDO::getInstance();
            if($request = $DB->prepare($SQL)) {
                if($request->execute([':id'=>$id])) {
                    if($request->rowCount() != 1) {
                        echo "error";
                    } else {
                        $forum=$request->fetch();
                        $forum_obj =  new Forum($forum['title'], $forum['category']);
                        $forum_obj->setId($forum['id']);

                        return $forum_obj;
                    }
                }
            }

            return null;
        }

        public static function updateForum(Forum $forum) : bool {
            $SQL = <<<SQL
                UPDATE forum SET title=:title, category=:category WHERE id=:id
            SQL;

            $DB = MyPDO::getInstance();
            if($request = $DB->prepare($SQL)) {
                return $request->execute([
                    ':title'=>$forum->getTitle(),
                    ':category'=>$forum->getCategory(),
                    ':id'=>$forum->getId()
                ]);
            }
            return false;
        }

        public static function deleteForum(int $id) : bool {
            $SQL = <<<SQL
                DELETE FROM forum WHERE id=:id
            SQL;

            $DB = MyPDO::getInstance();
            if($request = $DB->prepare($SQL)) {
                return $request->execute([':id'=>$id]);
            }
            return false;
        }

        public static function getForums() : array {
            $SQL = <<<SQL
                SELECT forum.id, forum.title, category.name FROM forum JOIN category ON forum.category = category.id ORDER BY forum.title DESC; 
            SQL;

            $DB = MyPDO::getInstance();
            $forums = array();
            if($request = $DB->prepare($SQL)) {
                if($request->execute()) {
                    while($forum = $request->fetch()) {
                        $forum_array = array();
                        array_push($forum_array, $forum['id']);
                        array_push($forum_array, $forum['title']);
                        array_push($forum_array, $forum['name']);

                        array_push($forums, $forum_array);
                    }
                }
            }
            return $forums;
        }

    }

?>