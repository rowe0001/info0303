<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <title>TP4 - Welcome</title>
</head>
<body>
    <?php
        session_start();

        if(isset($_POST['logout'])) {
            $_SESSION['connected'] = -1;
        }

        if(!isset($_SESSION['connected']) || $_SESSION['connected'] == -1) {
            header("Location: login.php");
        }

        if(isset($_POST['new_forum'])) {
            header("Location: create.php");
        }
    ?>
    <h1>Bienvenue!</h1>
    <form action="#" method="POST">
        <input type="submit" value="Logout" name="logout">
        <input type="submit" value="New Forum" name="new_forum">
    </form>
    <?php
        require_once 'class/ForumModel.php';
        
        echo "<ul>";
        foreach (ForumModel::getForums() as $forum) {
            echo "<li><a href=\"forums.php?id=".$forum[0]."\">".$forum[1]." - (#".$forum[2].")</a></li>";
        }
        echo "</ul>";
    ?>
    <p id="result"></p>
    <script>
        $(document).ready(function() {
            $.ajax({
                url: "rand.php",
                type: "GET",
                success: function(response) {
                    $("#result").html(response);
                },
                error: function(xhr, status, error) {
                    console.error("Error: " + error);
                }
            });
        });
    </script>

</body>
</html>