<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Title Test</h1>

    <p class="Exercice 1">
        <?php
        define("TABLE", [[1, 2], [3, 4]]);

        function displayTable(array $array) : string {
            $table = "<table style=\"border: 1px solid black;\">";
            foreach ($array as $value) {
                $table = $table."<tr style=\"border: 1px solid black;\">";
                foreach($value as $var) {
                    $table = $table."<td style=\"border: 1px solid black;\">".$var."</td>";
                }
                $table = $table."</tr>";
            }
            $table = $table."</table>";

            return $table;
        }

        echo displayTable(constant("TABLE"));
        ?>
    </p>

    <p class="Exercice 1.bis">
        <?php
        $t = [
            ["Nom" => "Eastwood", "Prenom" => "Clint", "Age" => 62],
            ["Nom" => "Gims", "Prenom"=>"Maître","Age" => 34],
            ["Nom" => "O'Neil", "Prenom"=>"Ed", "Age" => 74]
        ];

        function displayTable2(array $titles, array $values) : string {
            $table = "<table style=\"border: 1px solid black;\"><tr>";
            foreach($titles as $title) {
                $table = $table."<th style=\"border: 1px solid black;\">".$title."</th>";
            }
            $table = $table."</tr>";

            foreach($values as $value) {
                $table = $table."<tr style=\"border: 1px solid black;\">";
                foreach($value as $key => $val) {
                    $table=$table."<td style=\"border: 1px solid black;\">".$value[$key]."</td>";
                }
                $table = $table."</tr>";
            }
            $table = $table."<table>";

            return $table;
        }

        echo displayTable2(["Nom", "Prenom", "Age"], $t);
        ?>
    </p>

    <p class="Exercice 2">
        <?php
        define("TABLEAU", [1, 2, 3, 4, 5]);
        define("STRING", "1-2-3-4-5");

        function array2string(array $array, string $separator=";") : string {
            $result = "<p>";
            for($i = 0; $i < sizeof($array)-1; $i++) {
                $result = $result.$array[$i].$separator;
            }
            $result = $result.$array[sizeof($array)-1];
            return $result."</p>";
        }

        function string2array(string $string, string $separator=";") : array {
            $result = array();
            
            $i = 0;
            foreach(str_split($string) as $char) {
                if($char == $separator) {
                    array_push($result, $string[$i-1]);
                }
                $i = $i+1;
            }
            array_push($result, $string[strlen($string)-1]);
            return $result;
        }

        echo array2string(constant("TABLEAU"), "-");
        echo string2array(constant("STRING"), "-");
        ?>
    </p>

    <p class="Exercice 3">
        <?php
        function generateRandArray(int $size, int $bound1, int $bound2) : array {
            $array = array();

            for($i = 0; $i < $size; $i++) {
                array_push($array, rand($bound1, $bound2));
            }

            return $array;
        }

        function isPrime(int $number) : bool {
            if ($number == 1)
                return FALSE;
            
            for ($i = 2; $i <= $number/2; $i++){
                if ($number % $i == 0) {
                    return FALSE;
                }
            }
            return TRUE;
        }

        $array = generateRandArray(50, 0, 20);
        $array = array_filter($array, "isPrime");
        sort($array);
        $array = array_slice($array, 0, 10);
        $array = array_unique($array);

        echo array2string($array, " ");
        ?>
    </p>
</body>
</html> 