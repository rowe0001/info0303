@extends('template')

@section('title')
    PAGE 1
@endsection

@section('content')
    @php
        //warning("Test");

        /*try {
            throw new Exception("un exemple d'exception");
        } catch (Exception $e) {
            Debugbar::addThrowable($e);
        }*/

        Debugbar::startMeasure('render','Exemple 1');
        Debugbar::stopMeasure('render');
        Debugbar::addMeasure('maintenant', LARAVEL_START, microtime(true));
        Debugbar::measure('Une longue opération', function() {
            $cpt = 0;
            for($i = 0; $i < 1000; $i++)
            for($j = 0; $j < 1000; $j++)
                $cpt += $i + $j;
        });
    @endphp

    <a href="/"><button>Button</button></a>
@endsection
