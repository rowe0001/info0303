<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP4 - Forums</title>
</head>
<body>
    <?php
        require_once 'class/ForumModel.php';
        require_once 'class/MessageModel.php';

        session_start();

        if(!isset($_SESSION['connected']) || isset($_SESSION['connected']) && $_SESSION['connected'] == -1) {
            header("Location: login.php");
        }

        if(isset($_POST['message'])) {
            MessageModel::createMessage(new Message(
                $_SESSION['connected'],
                $_GET['id'],
                $_POST['message']
            ));
        }

        if(isset($_GET['id'])) {
            $forum=ForumModel::getForum($_GET['id']);
            echo "<h1>".$forum->getTitle()."</h1>";
            echo "<h3>Messages</h3>";
            echo "<ul>";
            
            foreach(MessageModel::getMessages($_GET['id']) as $message) {
                if($message[2] == $_SESSION['connected']) {
                    echo "<li>".$message[0].": ".$message[1]."<br><a href=\"edit.php?id=".$message[3]."\"><button>Edit</button></a></li>";
                } else {
                    echo "<li>".$message[0].": ".$message[1]."</li>";
                }
            }
            echo"</ul>";
            
        } else {
            header("Location: index.php");
        }
    ?>

    <form action="#" method="POST">
        <label for="message">Contribute to the conversation...</label>
        <input style="display: block;" type="text" name="message" value="">
    </form>

    <a href="index.php"><button style="margin-top: 50px;">Home</button></a>
</body>
</html>