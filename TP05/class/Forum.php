<?php
    final class Forum {
        private int $id;
        private string $title;
        private int $category_id;

        function __construct(string $title, int $category_id) {
            $this->id = -1;
            $this->title = $title;
            $this->category_id = $category_id;
        }

        public function getId() : int {
            return $this->id;
        }

        public function getTitle() : string {
            return $this->title;
        }

        public function getCategory() : int {
            return $this->category_id;
        }

        public function setId(int $id) {
            $this->id = $id;
        }
    }