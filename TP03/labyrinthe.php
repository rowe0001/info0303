<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Labyrinthe</h1>

    <p class="Exercice 1">
    <?php
        session_start();

        define("LABY", [
            [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
            [ 1, 0, 0, 0, 0, 1, 0, 0, 0, 1 ],
            [ 1, 0, 1, 1, 0, 1, 0, 1, 1, 1 ],
            [ 1, 0, 0, 1, 0, 1, 0, 1, 2, 1 ],
            [ 1, 1, 0, 1, 0, 1, 0, 0, 0, 1 ],
            [ 1, 0, 0, 1, 1, 1, 0, 1, 1, 1 ],
            [ 1, 0, 1, 1, 0, 0, 0, 0, 0, 1 ],
            [ 1, 0, 1, 1, 0, 1, 0, 1, 0, 1 ],
            [ 1, 0, 0, 0, 0, 1, 0, 1, 0, 1 ],
            [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ]
        ]);

        function init() : void {
            $_SESSION['posX'] = 1;
            $_SESSION['posY'] = 1;
            $_SESSION['moves'] = [[1, 1]];      
        }

        function displayForm() : void {
            $form = "<form action=\"#\" method=\"POST\">";
            $form = $form."    <input type=\"submit\" name=\"Reset\" value=\"Reset\">";

            if(LABY[$_SESSION['posX']-1][$_SESSION['posY']] == 1) {
                $form = $form."    <input type=\"submit\" value=\"Haut\" disabled>";
            } else {
                $form = $form."    <input type=\"submit\" name=\"Gauche\" value=\"Haut\">";
            }

            if(LABY[$_SESSION['posX']][$_SESSION['posY']-1] == 1) {
                $form = $form."    <input type=\"submit\" value=\"Gauche\" disabled>";
            } else {
                $form = $form."    <input type=\"submit\" name=\"Haut\" value=\"Gauche\">";
            }

            if(LABY[$_SESSION['posX']][$_SESSION['posY']+1] == 1) {
                $form = $form."    <input type=\"submit\" value=\"Droite\" disabled>";
            } else {
                $form = $form."    <input type=\"submit\" name=\"Bas\" value=\"Droite\">";
            }

            if(LABY[$_SESSION['posX']+1][$_SESSION['posY']] == 1) {
                $form = $form."    <input type=\"submit\" value=\"Bas\" disabled>";
            } else {
                $form = $form."    <input type=\"submit\" name=\"Droite\" value=\"Bas\">";
            }
            
            $form = $form."</form>";

            if(LABY[$_SESSION['posX']][$_SESSION['posY']] == 2) {
                $form = "Bravo!";
                $form = $form."<form action=\"#\" method=\"POST\">";
                $form = $form."    <input type=\"submit\" name=\"Reset\" value=\"Reset\">";
                $form = $form."</form>";
            }

            echo $form;
        }

        function displayTable(array $array) : void {
            foreach($_SESSION['moves'] as $move) {
                $array[$move[0]][$move[1]] = 4;
            }

            $array[$_SESSION['posX']][$_SESSION['posY']] = 3;

            $table = "<table>";
            foreach($array as $value) {
                $table = $table."<tr>";
                foreach($value as $var) {
                    
                    //0 : Walkable
                    //1 : Wall
                    //2 : Finish
                    //3 : Player
                    //4 : Previous move

                    if($var == 0) {
                        $emoji = " ";
                    } else if($var == 1) {
                        $emoji = "⬛️";
                    } else if($var == 2) {
                        $emoji = "🏁";
                    } else if($var == 3) {
                        $emoji = "🐸";
                    } else if($var == 4) {
                        $emoji = "🔸";
                    } else {
                        $emoji = $var;
                    }

                    $table = $table."<td>".$emoji."</td>";
                }
                $table = $table."</tr>";
            }
            $table = $table."</table>";

            echo $table;
        }
    
        if(!isset($_SESSION['posX']) || !isset($_SESSION['posY'])) {
            init();
        }

        if(isset($_POST['Gauche']) && LABY[$_SESSION['posX']-1][$_SESSION['posY']] != 1) {
            $_SESSION['posX'] -= 1;
        } else if(isset($_POST['Haut']) && LABY[$_SESSION['posX']][$_SESSION['posY']-1] != 1) {
            $_SESSION['posY'] -= 1;
        } else if(isset($_POST['Bas']) && LABY[$_SESSION['posX']][$_SESSION['posY']+1] != 1) {
            $_SESSION['posY'] += 1;
        } else if(isset($_POST['Droite']) && LABY[$_SESSION['posX']+1][$_SESSION['posY']] != 1) {
            $_SESSION['posX'] += 1;
        } else if(isset($_POST['Reset'])) {
            init();
        }

        if(end($_SESSION['moves']) != array($_SESSION['posX'], $_SESSION['posY'])) {
            array_push($_SESSION['moves'], array($_SESSION['posX'], $_SESSION['posY']));
        }
        
        displayTable(LABY);
        displayForm();
    ?>
    </p>
</body>
</html>
