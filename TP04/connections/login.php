<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
</head>
<body>
    <?php
        include 'classes/MyPDO.php';

        session_start();

        if(isset($_POST['login']) && isset($_POST['password'])) {
            
            $DB = MyPDO::getInstance();
            $SQL = <<<SQL
                SELECT login, password FROM user WHERE login=:login AND password=:password
            SQL;

            if($request = $DB->prepare($SQL)) {
                if($request->execute([':login'=>$_POST['login'], ':password'=>$_POST['password']])) {
                    $rowCount = $request->rowCount();
                    if($rowCount == 1) {
                        $_SESSION['connected'] = true;
                        header("Location: index.php");
                    }
                    echo "Incorrect Login or Password";
                }
            }

        }
    ?>
    <div style="margin: auto; width: 50%; border: 1px solid black; padding: 10px;">
        <form style="form { margin: 0 auto; }" action="#" method="POST">
            <label for="login">Login</label>
            <input style="display: block;" type="text" name="login" value="">
            <label for="password">Password</label>
            <input style="display: block;" type="text" name="password" value="">
            <input style="display: block;" type="submit" value="Submit">
        </form>
    </div>
</body>
</html>