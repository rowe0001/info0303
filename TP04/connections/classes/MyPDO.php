<?php
    include 'config/DBConfig.php';

    final class MyPDO {

        private static ?PDO $instance = null;
        private const OPTIONS = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ];

        public static function getInstance() : PDO {

            if (self::$instance === null) {
                self::$instance = new PDO(
                    "mysql:host=".HOST.";dbname=".NAME.";charset=utf8",
                    USERNAME,
                    PASSWORD,
                    self::OPTIONS
                );
            }
            return self::$instance;
        }

    }
?>