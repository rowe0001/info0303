<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Title Test</h1>
    <p class="Exercice 1"> <?php echo "coucou"; ?> </p>
    
    <p class="Exercice 2">
        <?php
        $sum = 0;
        for($i = 0; $i <= 10; $i++) {
            $sum += $i;
        }

        echo "Sum is : ".$sum; 
        ?>
    </p>

    <p class="Exercice 3">
        <?php
        $fmt = datefmt_create(
            'fr-FR',
            IntlDateFormatter::LONG,
            IntlDateFormatter::LONG,
            'Europe/Paris',
            IntlDateFormatter::GREGORIAN,
        );
        echo 'Bienvenue! Nous sommes le ' .datefmt_format($fmt, strtotime('now'));
        ?>
    </p>
</body>
</html> 