<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP4 - Create</title>
</head>
<body>
    <?php
        session_start();

        if(isset($_SESSION['connected']) && $_SESSION['connected'] == -1) {
            header("Location: login.php");
        }
    ?>

    <h1>Create Forum</h1>
    <form action="#" method="POST">
        <label for="title">Forum Title</label>
        <input style="margin-bottom: 10px;" type="text" name="title" value="">
        
        <label style="display: block;" for="category">Category</label>
        <select style="display: block; margin-bottom: 50px;" name="category" size="5">
            <option value="1">Fashion</option>
            <option value="2">Video Games</option>
            <option value="3">Tech</option>
            <option value="4">Programming</option>
            <option value="5">Linux</option>
        </select>

        <input type="submit" value="Create">
    </form>
    <?php
        require_once 'class/ForumModel.php';

        if(isset($_POST['title']) && isset($_POST['category'])) {
            $id = ForumModel::createForum(new Forum($_POST['title'], $_POST['category']));
            header("Location: forums.php?id=".$id);
        }
    ?>

    <a href="index.php"><button style="margin-top: 50px;">Home</button></a>
</body>
</html>