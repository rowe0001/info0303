<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Cookie</h1>

    <p class="Exercice 1">
        <?php
            require_once "./state.php";
            require_once "./delete.php";

            if(cookieExists()) {
                var_dump($_COOKIE);
                echo "<br>";
                deleteCookie();
                var_dump($_COOKIE);

                echo "<br>Je vous connais<br>";
            } else {
                var_dump($_COOKIE);
                echo "<br>";
                setcookie("visited", "true", time()+60);
                var_dump($_COOKIE);
                
                echo "<br>Je vous connaissais pas, mais maintenant si!<br>";
            }
        ?>
    </p>
</body>
</html>
