<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP4 - Forums</title>
</head>
<body>
    <?php
        require_once 'class/ForumModel.php';
        require_once 'class/MessageModel.php';

        if(isset($_GET['id'])) {
            $forum=ForumModel::getForum($_GET['id']);
            echo "<h1>".$forum->getTitle()."</h1>";
            echo "<h3>Messages</h3>";
            echo "<ul>";
            
            $i = 0;
            foreach(MessageModel::getMessages($_GET['id']) as $message) {
                echo "<li>".$message[0].": ".$message[1]."</li>";
                $i+=1;
            }
            echo"</ul>";
            
        } else {
            header("Location: index.php");
        }
    ?>
</body>
</html>