<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemController extends Controller {

    public function index() {
        return view('index');
    }

    public function show($n) {
        return view('item')->with('number', $n);
    }
}
