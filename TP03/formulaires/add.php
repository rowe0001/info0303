<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add</title>
</head>
<body>
    <a href="./index.php">Return</a>

    <?php
        session_start();

        if(file_exists("./db.txt") && isset($_POST['input']) && isset($_SESSION['id']) && isset($_POST['idcheck']) && $_POST['idcheck'] == $_SESSION['id']) {
            $current = file_get_contents("./db.txt");
            $current .= $_POST['input']."\n";
            file_put_contents("./db.txt", $current);

            unset($_SESSION['id']);

            echo "Enregistrement Ajoute.";
        }

        $id = uniqid();
        $_SESSION['id'] = $id;

        echo <<<HTML
            <form action="#" method="POST">
                <label for="input">Enter text...</label>
                <input type="text" name="input" id="input">
                <input type="submit" value="Send">
                <input type="hidden" name="idcheck" value="$id">
            </form>
        HTML;
    ?>
</body>
</html>