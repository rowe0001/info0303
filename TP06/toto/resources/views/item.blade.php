@extends('template')

@section('title')
    ITEM
@endsection

@section('content')
    @php
        echo $number;
    @endphp

    @if ($number > 1)
        <br>
        <a href="{{$number-1}}">Article n°{{$number-1}}</a>
    @endif
@endsection