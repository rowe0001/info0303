# INFO0303 - Laravel

Il s'agit d'une application *Laravel* de base, avec la barre de debug.

## Récupération du dépôt

```
git clone https://gitlab-mi.univ-reims.fr/rabat01/laravel laravel
cd laravel
composer install
cp .env.example .env
php artisan key:generate
php artisan storage:link
```

Remplacez `cp` par `copy` si vous êtes sous *Windows*.