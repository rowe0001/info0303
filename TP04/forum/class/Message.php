<?php
    final class Message {
        private int $id;
        private int $idF;
        private int $idU;
        private string $message;

        function __construct(int $id, int $idF, int $idU, string $message) {
            $this->id = $id;
            $this->idU = $idU;
            $this->idF = $idF;
            $this->message = $message;
        }

        public function getId() : int {
            return $this->id;
        }

        public function getIdF() : int {
            return $this->idU;
        }

        public function getIdU() : int {
            return $this->idF;
        }

        public function getMessage() : string {
            return $this->message;
        }
    }