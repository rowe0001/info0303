<?php
    final class Message {
        private int $id;
        private int $idF;
        private int $idU;
        private string $message;

        function __construct(int $idF, int $idU, string $message) {
            $this->id = -1;
            $this->idU = $idU;
            $this->idF = $idF;
            $this->message = $message;
        }

        public function getId() : int {
            return $this->id;
        }

        public function getIdF() : int {
            return $this->idF;
        }

        public function getIdU() : int {
            return $this->idU;
        }

        public function getMessage() : string {
            return $this->message;
        }

        public function setId(int $id) : void {
            $this->id = $id;
        }
    }