<?php

use App\Http\Controllers\ItemController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [ItemController::class, 'index']);

Route::get('/page1', function () {
    return view('page1');
});

Route::get('item/{n}', [App\Http\Controllers\ItemController::class, 'show']
)->where('n', '[0-9]+')->name('item.show');

Route::fallback(function () {
    return view('error');
});