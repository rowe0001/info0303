<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP4 - Edit</title>
</head>
<body>
    <?php
        require_once 'class/MessageModel.php';
        require_once 'class/MyPDO.php';

        session_start();

        if(!isset($_SESSION['connected']) || isset($_SESSION['connected']) && $_SESSION['connected'] == -1) {
            header("Location: login.php");
        }

        if(!isset($_GET['id'])) {
            header("Location: index.php");
        }

        $message = MessageModel::getMessage($_GET['id']);
        $content = $message->getMessage();

        if($_SESSION['connected'] != $message->getIdU()) {
            header("Location: forums.php?id=".$message->getIdF());
        }
        
        if($_SESSION['connected'] == $message->getIdU() && isset($_POST['content'])) {
            $new_message = new Message($message->getIdF(), $message->getIdU(), $_POST['content']);
            $new_message->setId($_GET['id']);

            MessageModel::updateMessage($new_message);
            header("Location: forums.php?id=".$new_message->getIdF());
        }
    ?>
    <form action="#" method="POST">
        <label style="display: block;" for="content">Edit content</label>
        <input style="width: 800px;" type="text" name="content" value="<?php echo $content ?>">
    </form>
</body>
</html>