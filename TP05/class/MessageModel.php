<?php
    include 'class/Message.php';

    final class MessageModel {

        public static function createMessage(Message $message) : int {
            $SQL = <<<SQL
                INSERT INTO message(idF, idU, message) VALUES(:idF, :idU, :message);
            SQL;

            $DB = MyPDO::getInstance();
            if($request = $DB->prepare($SQL)) {
                $request->execute([
                    ':idF'=>$message->getIdF(),
                    ':idU'=>$message->getIdU(),
                    ':message'=>$message->getMessage()
                ]);

                return $DB->lastInsertId();
            }

            return -1;
        }

        public static function getMessage(int $id) : ?Message{
            $SQL = <<<SQL
                SELECT * FROM message WHERE id=:id
            SQL;

            $DB = MyPDO::getInstance();
            if($request = $DB->prepare($SQL)) {
                if($request->execute([':id'=>$id])) {
                    if($request->rowCount() != 1) {
                        echo "error";
                    } else {
                        $message=$request->fetch();
                        return new Message(
                            $message['idF'],
                            $message['idU'],
                            $message['message']
                        );
                    }
                }
            }

            return null;
        }

        public static function updateMessage(Message $message) : bool {
            $SQL = <<<SQL
                UPDATE message SET message=:message WHERE id=:id
            SQL;

            $DB = MyPDO::getInstance();
            if($request = $DB->prepare($SQL)) {
                return $request->execute([
                    ':message'=>$message->getMessage(), 
                    ':id'=>$message->getId()
                ]);
            }
            return false;
        }

        public static function deleteMessage(int $id) : bool {
            $SQL = <<<SQL
                DELETE FROM message WHERE id=:id
            SQL;

            $DB = MyPDO::getInstance();
            if($request = $DB->prepare($SQL)) {
                return $request->execute([':id'=>$id]);
            }
            return false;
        }

        public static function getMessages(int $id) : array {
            $SQL = <<<SQL
                SELECT message.id AS id_message, user.id, user.login, message.message FROM message JOIN user ON message.idU = user.id WHERE message.idF=:id ORDER BY message.message DESC; 
            SQL;

            $DB = MyPDO::getInstance();
            $messages = array();
            if($request = $DB->prepare($SQL)) {
                if($request->execute([':id'=>$id])) {
                    while($message = $request->fetch()) {
                        $message_array = array();
                        array_push($message_array, $message['login']);
                        array_push($message_array, $message['message']);
                        array_push($message_array, $message['id']);
                        array_push($message_array, $message['id_message']);

                        array_push($messages, $message_array);
                    }
                }
            }
            return $messages;
        }

    }